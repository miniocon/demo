package io.apext0.demo.api;

import org.keycloak.KeycloakPrincipal;
import org.keycloak.adapters.springsecurity.token.KeycloakAuthenticationToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.Arrays;

@Controller
class PersonController {

    final static Logger LOG = LoggerFactory.getLogger(PersonController.class);


    @RequestMapping("/user")
    public ResponseEntity index(KeycloakAuthenticationToken p) {
        LOG.debug("p:{}", p != null ? p.getName() : "null");
        return ResponseEntity.ok(p.getName());
    }

    @GetMapping(path = "/")
    public String getIndex() {
        return "index";
    }

    @GetMapping(path = "/persons")
    public String getPersons(Model model) {
        model.addAttribute("persons", Arrays.asList("John", "David", "Peter"));
        return "persons";
    }

    @GetMapping(path = "/logout")
    public String logout(HttpServletRequest request) throws ServletException {
        request.logout();
        return "/";
    }
}